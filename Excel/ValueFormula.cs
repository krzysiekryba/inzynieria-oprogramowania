﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excel
{
    class ValueFormula
    {
        public ValueFormula(string value, string formula, string row, string col)
        {
            this.value = value;
            this.formula = formula;
            this.row = row;
            this.col = col;
        }


        private string value;
        private string formula;
        private string row;
        private string col;
    }
}
