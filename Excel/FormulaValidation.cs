﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excel
{
    class FormulaValidation
    {
        public bool IsFormula(string text)
        {
            if (text.Substring(0, 1) == "=")
                return true;
            else
                return false;
        }
        public bool IsNumber(string text)
        {
            int.TryParse(text, out int n);
            return Convert.ToBoolean(n);
        }
    }
}
