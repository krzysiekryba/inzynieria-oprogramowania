﻿namespace Excel
{
    partial class ExcelForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.formulaBox = new System.Windows.Forms.TextBox();
            this.A1 = new System.Windows.Forms.TextBox();
            this.B1 = new System.Windows.Forms.TextBox();
            this.D1 = new System.Windows.Forms.TextBox();
            this.C1 = new System.Windows.Forms.TextBox();
            this.H1 = new System.Windows.Forms.TextBox();
            this.G1 = new System.Windows.Forms.TextBox();
            this.F1 = new System.Windows.Forms.TextBox();
            this.E1 = new System.Windows.Forms.TextBox();
            this.J1 = new System.Windows.Forms.TextBox();
            this.I1 = new System.Windows.Forms.TextBox();
            this.J2 = new System.Windows.Forms.TextBox();
            this.I2 = new System.Windows.Forms.TextBox();
            this.H2 = new System.Windows.Forms.TextBox();
            this.G2 = new System.Windows.Forms.TextBox();
            this.F2 = new System.Windows.Forms.TextBox();
            this.E2 = new System.Windows.Forms.TextBox();
            this.D2 = new System.Windows.Forms.TextBox();
            this.C2 = new System.Windows.Forms.TextBox();
            this.B2 = new System.Windows.Forms.TextBox();
            this.A2 = new System.Windows.Forms.TextBox();
            this.J4 = new System.Windows.Forms.TextBox();
            this.I4 = new System.Windows.Forms.TextBox();
            this.H4 = new System.Windows.Forms.TextBox();
            this.G4 = new System.Windows.Forms.TextBox();
            this.F4 = new System.Windows.Forms.TextBox();
            this.E4 = new System.Windows.Forms.TextBox();
            this.D4 = new System.Windows.Forms.TextBox();
            this.C4 = new System.Windows.Forms.TextBox();
            this.B4 = new System.Windows.Forms.TextBox();
            this.A4 = new System.Windows.Forms.TextBox();
            this.J3 = new System.Windows.Forms.TextBox();
            this.I3 = new System.Windows.Forms.TextBox();
            this.H3 = new System.Windows.Forms.TextBox();
            this.G3 = new System.Windows.Forms.TextBox();
            this.F3 = new System.Windows.Forms.TextBox();
            this.E3 = new System.Windows.Forms.TextBox();
            this.D3 = new System.Windows.Forms.TextBox();
            this.C3 = new System.Windows.Forms.TextBox();
            this.B3 = new System.Windows.Forms.TextBox();
            this.A3 = new System.Windows.Forms.TextBox();
            this.J6 = new System.Windows.Forms.TextBox();
            this.I6 = new System.Windows.Forms.TextBox();
            this.H6 = new System.Windows.Forms.TextBox();
            this.G6 = new System.Windows.Forms.TextBox();
            this.F6 = new System.Windows.Forms.TextBox();
            this.E6 = new System.Windows.Forms.TextBox();
            this.D6 = new System.Windows.Forms.TextBox();
            this.C6 = new System.Windows.Forms.TextBox();
            this.B6 = new System.Windows.Forms.TextBox();
            this.A6 = new System.Windows.Forms.TextBox();
            this.J5 = new System.Windows.Forms.TextBox();
            this.I5 = new System.Windows.Forms.TextBox();
            this.H5 = new System.Windows.Forms.TextBox();
            this.G5 = new System.Windows.Forms.TextBox();
            this.F5 = new System.Windows.Forms.TextBox();
            this.E5 = new System.Windows.Forms.TextBox();
            this.D5 = new System.Windows.Forms.TextBox();
            this.C5 = new System.Windows.Forms.TextBox();
            this.B5 = new System.Windows.Forms.TextBox();
            this.A5 = new System.Windows.Forms.TextBox();
            this.J8 = new System.Windows.Forms.TextBox();
            this.I8 = new System.Windows.Forms.TextBox();
            this.H8 = new System.Windows.Forms.TextBox();
            this.G8 = new System.Windows.Forms.TextBox();
            this.F8 = new System.Windows.Forms.TextBox();
            this.E8 = new System.Windows.Forms.TextBox();
            this.D8 = new System.Windows.Forms.TextBox();
            this.C8 = new System.Windows.Forms.TextBox();
            this.B8 = new System.Windows.Forms.TextBox();
            this.A8 = new System.Windows.Forms.TextBox();
            this.J7 = new System.Windows.Forms.TextBox();
            this.I7 = new System.Windows.Forms.TextBox();
            this.H7 = new System.Windows.Forms.TextBox();
            this.G7 = new System.Windows.Forms.TextBox();
            this.F7 = new System.Windows.Forms.TextBox();
            this.E7 = new System.Windows.Forms.TextBox();
            this.D7 = new System.Windows.Forms.TextBox();
            this.C7 = new System.Windows.Forms.TextBox();
            this.B7 = new System.Windows.Forms.TextBox();
            this.A7 = new System.Windows.Forms.TextBox();
            this.J10 = new System.Windows.Forms.TextBox();
            this.I10 = new System.Windows.Forms.TextBox();
            this.H10 = new System.Windows.Forms.TextBox();
            this.G10 = new System.Windows.Forms.TextBox();
            this.F10 = new System.Windows.Forms.TextBox();
            this.E10 = new System.Windows.Forms.TextBox();
            this.D10 = new System.Windows.Forms.TextBox();
            this.C10 = new System.Windows.Forms.TextBox();
            this.B10 = new System.Windows.Forms.TextBox();
            this.A10 = new System.Windows.Forms.TextBox();
            this.J9 = new System.Windows.Forms.TextBox();
            this.I9 = new System.Windows.Forms.TextBox();
            this.H9 = new System.Windows.Forms.TextBox();
            this.G9 = new System.Windows.Forms.TextBox();
            this.F9 = new System.Windows.Forms.TextBox();
            this.E9 = new System.Windows.Forms.TextBox();
            this.D9 = new System.Windows.Forms.TextBox();
            this.C9 = new System.Windows.Forms.TextBox();
            this.B9 = new System.Windows.Forms.TextBox();
            this.A9 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otwórzToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zapiszToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // formulaBox
            // 
            this.formulaBox.Location = new System.Drawing.Point(130, 95);
            this.formulaBox.Name = "formulaBox";
            this.formulaBox.Size = new System.Drawing.Size(633, 20);
            this.formulaBox.TabIndex = 0;
            this.formulaBox.TextChanged += new System.EventHandler(this.formulaBox_TextChanged);
            // 
            // A1
            // 
            this.A1.Location = new System.Drawing.Point(59, 152);
            this.A1.Name = "A1";
            this.A1.Size = new System.Drawing.Size(65, 20);
            this.A1.TabIndex = 1;
            this.A1.Click += new System.EventHandler(this.SelectCell);
            // 
            // B1
            // 
            this.B1.Location = new System.Drawing.Point(130, 152);
            this.B1.Name = "B1";
            this.B1.Size = new System.Drawing.Size(65, 20);
            this.B1.TabIndex = 2;
            this.B1.Click += new System.EventHandler(this.SelectCell);
            // 
            // D1
            // 
            this.D1.Location = new System.Drawing.Point(272, 152);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(65, 20);
            this.D1.TabIndex = 4;
            this.D1.Click += new System.EventHandler(this.SelectCell);
            // 
            // C1
            // 
            this.C1.Location = new System.Drawing.Point(201, 152);
            this.C1.Name = "C1";
            this.C1.Size = new System.Drawing.Size(65, 20);
            this.C1.TabIndex = 3;
            this.C1.Click += new System.EventHandler(this.SelectCell);
            // 
            // H1
            // 
            this.H1.Location = new System.Drawing.Point(556, 152);
            this.H1.Name = "H1";
            this.H1.Size = new System.Drawing.Size(65, 20);
            this.H1.TabIndex = 8;
            this.H1.Click += new System.EventHandler(this.SelectCell);
            // 
            // G1
            // 
            this.G1.Location = new System.Drawing.Point(485, 152);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(65, 20);
            this.G1.TabIndex = 7;
            this.G1.Click += new System.EventHandler(this.SelectCell);
            // 
            // F1
            // 
            this.F1.Location = new System.Drawing.Point(414, 152);
            this.F1.Name = "F1";
            this.F1.Size = new System.Drawing.Size(65, 20);
            this.F1.TabIndex = 6;
            this.F1.Click += new System.EventHandler(this.SelectCell);
            // 
            // E1
            // 
            this.E1.Location = new System.Drawing.Point(343, 152);
            this.E1.Name = "E1";
            this.E1.Size = new System.Drawing.Size(65, 20);
            this.E1.TabIndex = 5;
            this.E1.Click += new System.EventHandler(this.SelectCell);
            // 
            // J1
            // 
            this.J1.Location = new System.Drawing.Point(698, 152);
            this.J1.Name = "J1";
            this.J1.Size = new System.Drawing.Size(65, 20);
            this.J1.TabIndex = 10;
            this.J1.Click += new System.EventHandler(this.SelectCell);
            // 
            // I1
            // 
            this.I1.Location = new System.Drawing.Point(627, 152);
            this.I1.Name = "I1";
            this.I1.Size = new System.Drawing.Size(65, 20);
            this.I1.TabIndex = 9;
            this.I1.Click += new System.EventHandler(this.SelectCell);
            // 
            // J2
            // 
            this.J2.Location = new System.Drawing.Point(698, 178);
            this.J2.Name = "J2";
            this.J2.Size = new System.Drawing.Size(65, 20);
            this.J2.TabIndex = 20;
            this.J2.Click += new System.EventHandler(this.SelectCell);
            // 
            // I2
            // 
            this.I2.Location = new System.Drawing.Point(627, 178);
            this.I2.Name = "I2";
            this.I2.Size = new System.Drawing.Size(65, 20);
            this.I2.TabIndex = 19;
            this.I2.Click += new System.EventHandler(this.SelectCell);
            // 
            // H2
            // 
            this.H2.Location = new System.Drawing.Point(556, 178);
            this.H2.Name = "H2";
            this.H2.Size = new System.Drawing.Size(65, 20);
            this.H2.TabIndex = 18;
            this.H2.Click += new System.EventHandler(this.SelectCell);
            // 
            // G2
            // 
            this.G2.Location = new System.Drawing.Point(485, 178);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(65, 20);
            this.G2.TabIndex = 17;
            this.G2.Click += new System.EventHandler(this.SelectCell);
            // 
            // F2
            // 
            this.F2.Location = new System.Drawing.Point(414, 178);
            this.F2.Name = "F2";
            this.F2.Size = new System.Drawing.Size(65, 20);
            this.F2.TabIndex = 16;
            this.F2.Click += new System.EventHandler(this.SelectCell);
            // 
            // E2
            // 
            this.E2.Location = new System.Drawing.Point(343, 178);
            this.E2.Name = "E2";
            this.E2.Size = new System.Drawing.Size(65, 20);
            this.E2.TabIndex = 15;
            this.E2.Click += new System.EventHandler(this.SelectCell);
            // 
            // D2
            // 
            this.D2.Location = new System.Drawing.Point(272, 178);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(65, 20);
            this.D2.TabIndex = 14;
            this.D2.Click += new System.EventHandler(this.SelectCell);
            // 
            // C2
            // 
            this.C2.Location = new System.Drawing.Point(201, 178);
            this.C2.Name = "C2";
            this.C2.Size = new System.Drawing.Size(65, 20);
            this.C2.TabIndex = 13;
            this.C2.Click += new System.EventHandler(this.SelectCell);
            // 
            // B2
            // 
            this.B2.Location = new System.Drawing.Point(130, 178);
            this.B2.Name = "B2";
            this.B2.Size = new System.Drawing.Size(65, 20);
            this.B2.TabIndex = 12;
            this.B2.Click += new System.EventHandler(this.SelectCell);
            // 
            // A2
            // 
            this.A2.Location = new System.Drawing.Point(59, 178);
            this.A2.Name = "A2";
            this.A2.Size = new System.Drawing.Size(65, 20);
            this.A2.TabIndex = 11;
            this.A2.Click += new System.EventHandler(this.SelectCell);
            // 
            // J4
            // 
            this.J4.Location = new System.Drawing.Point(698, 230);
            this.J4.Name = "J4";
            this.J4.Size = new System.Drawing.Size(65, 20);
            this.J4.TabIndex = 40;
            this.J4.Click += new System.EventHandler(this.SelectCell);
            // 
            // I4
            // 
            this.I4.Location = new System.Drawing.Point(627, 230);
            this.I4.Name = "I4";
            this.I4.Size = new System.Drawing.Size(65, 20);
            this.I4.TabIndex = 39;
            this.I4.Click += new System.EventHandler(this.SelectCell);
            // 
            // H4
            // 
            this.H4.Location = new System.Drawing.Point(556, 230);
            this.H4.Name = "H4";
            this.H4.Size = new System.Drawing.Size(65, 20);
            this.H4.TabIndex = 38;
            this.H4.Click += new System.EventHandler(this.SelectCell);
            // 
            // G4
            // 
            this.G4.Location = new System.Drawing.Point(485, 230);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(65, 20);
            this.G4.TabIndex = 37;
            this.G4.Click += new System.EventHandler(this.SelectCell);
            // 
            // F4
            // 
            this.F4.Location = new System.Drawing.Point(414, 230);
            this.F4.Name = "F4";
            this.F4.Size = new System.Drawing.Size(65, 20);
            this.F4.TabIndex = 36;
            this.F4.Click += new System.EventHandler(this.SelectCell);
            // 
            // E4
            // 
            this.E4.Location = new System.Drawing.Point(343, 230);
            this.E4.Name = "E4";
            this.E4.Size = new System.Drawing.Size(65, 20);
            this.E4.TabIndex = 35;
            this.E4.Click += new System.EventHandler(this.SelectCell);
            // 
            // D4
            // 
            this.D4.Location = new System.Drawing.Point(272, 230);
            this.D4.Name = "D4";
            this.D4.Size = new System.Drawing.Size(65, 20);
            this.D4.TabIndex = 34;
            this.D4.Click += new System.EventHandler(this.SelectCell);
            // 
            // C4
            // 
            this.C4.Location = new System.Drawing.Point(201, 230);
            this.C4.Name = "C4";
            this.C4.Size = new System.Drawing.Size(65, 20);
            this.C4.TabIndex = 33;
            this.C4.Click += new System.EventHandler(this.SelectCell);
            // 
            // B4
            // 
            this.B4.Location = new System.Drawing.Point(130, 230);
            this.B4.Name = "B4";
            this.B4.Size = new System.Drawing.Size(65, 20);
            this.B4.TabIndex = 32;
            this.B4.Click += new System.EventHandler(this.SelectCell);
            // 
            // A4
            // 
            this.A4.Location = new System.Drawing.Point(59, 230);
            this.A4.Name = "A4";
            this.A4.Size = new System.Drawing.Size(65, 20);
            this.A4.TabIndex = 31;
            this.A4.Click += new System.EventHandler(this.SelectCell);
            // 
            // J3
            // 
            this.J3.Location = new System.Drawing.Point(698, 204);
            this.J3.Name = "J3";
            this.J3.Size = new System.Drawing.Size(65, 20);
            this.J3.TabIndex = 30;
            this.J3.Click += new System.EventHandler(this.SelectCell);
            // 
            // I3
            // 
            this.I3.Location = new System.Drawing.Point(627, 204);
            this.I3.Name = "I3";
            this.I3.Size = new System.Drawing.Size(65, 20);
            this.I3.TabIndex = 29;
            this.I3.Click += new System.EventHandler(this.SelectCell);
            // 
            // H3
            // 
            this.H3.Location = new System.Drawing.Point(556, 204);
            this.H3.Name = "H3";
            this.H3.Size = new System.Drawing.Size(65, 20);
            this.H3.TabIndex = 28;
            this.H3.Click += new System.EventHandler(this.SelectCell);
            // 
            // G3
            // 
            this.G3.Location = new System.Drawing.Point(485, 204);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(65, 20);
            this.G3.TabIndex = 27;
            this.G3.Click += new System.EventHandler(this.SelectCell);
            // 
            // F3
            // 
            this.F3.Location = new System.Drawing.Point(414, 204);
            this.F3.Name = "F3";
            this.F3.Size = new System.Drawing.Size(65, 20);
            this.F3.TabIndex = 26;
            this.F3.Click += new System.EventHandler(this.SelectCell);
            // 
            // E3
            // 
            this.E3.Location = new System.Drawing.Point(343, 204);
            this.E3.Name = "E3";
            this.E3.Size = new System.Drawing.Size(65, 20);
            this.E3.TabIndex = 25;
            this.E3.Click += new System.EventHandler(this.SelectCell);
            // 
            // D3
            // 
            this.D3.Location = new System.Drawing.Point(272, 204);
            this.D3.Name = "D3";
            this.D3.Size = new System.Drawing.Size(65, 20);
            this.D3.TabIndex = 24;
            this.D3.Click += new System.EventHandler(this.SelectCell);
            // 
            // C3
            // 
            this.C3.Location = new System.Drawing.Point(201, 204);
            this.C3.Name = "C3";
            this.C3.Size = new System.Drawing.Size(65, 20);
            this.C3.TabIndex = 23;
            this.C3.Click += new System.EventHandler(this.SelectCell);
            // 
            // B3
            // 
            this.B3.Location = new System.Drawing.Point(130, 204);
            this.B3.Name = "B3";
            this.B3.Size = new System.Drawing.Size(65, 20);
            this.B3.TabIndex = 22;
            this.B3.Click += new System.EventHandler(this.SelectCell);
            // 
            // A3
            // 
            this.A3.Location = new System.Drawing.Point(59, 204);
            this.A3.Name = "A3";
            this.A3.Size = new System.Drawing.Size(65, 20);
            this.A3.TabIndex = 21;
            this.A3.Click += new System.EventHandler(this.SelectCell);
            // 
            // J6
            // 
            this.J6.BackColor = System.Drawing.SystemColors.Window;
            this.J6.Location = new System.Drawing.Point(698, 282);
            this.J6.Name = "J6";
            this.J6.Size = new System.Drawing.Size(65, 20);
            this.J6.TabIndex = 60;
            this.J6.Click += new System.EventHandler(this.SelectCell);
            // 
            // I6
            // 
            this.I6.Location = new System.Drawing.Point(627, 282);
            this.I6.Name = "I6";
            this.I6.Size = new System.Drawing.Size(65, 20);
            this.I6.TabIndex = 59;
            this.I6.Click += new System.EventHandler(this.SelectCell);
            // 
            // H6
            // 
            this.H6.Location = new System.Drawing.Point(556, 282);
            this.H6.Name = "H6";
            this.H6.Size = new System.Drawing.Size(65, 20);
            this.H6.TabIndex = 58;
            this.H6.Click += new System.EventHandler(this.SelectCell);
            // 
            // G6
            // 
            this.G6.Location = new System.Drawing.Point(485, 282);
            this.G6.Name = "G6";
            this.G6.Size = new System.Drawing.Size(65, 20);
            this.G6.TabIndex = 57;
            this.G6.Click += new System.EventHandler(this.SelectCell);
            // 
            // F6
            // 
            this.F6.Location = new System.Drawing.Point(414, 282);
            this.F6.Name = "F6";
            this.F6.Size = new System.Drawing.Size(65, 20);
            this.F6.TabIndex = 56;
            this.F6.Click += new System.EventHandler(this.SelectCell);
            // 
            // E6
            // 
            this.E6.Location = new System.Drawing.Point(343, 282);
            this.E6.Name = "E6";
            this.E6.Size = new System.Drawing.Size(65, 20);
            this.E6.TabIndex = 55;
            this.E6.Click += new System.EventHandler(this.SelectCell);
            // 
            // D6
            // 
            this.D6.Location = new System.Drawing.Point(272, 282);
            this.D6.Name = "D6";
            this.D6.Size = new System.Drawing.Size(65, 20);
            this.D6.TabIndex = 54;
            this.D6.Click += new System.EventHandler(this.SelectCell);
            // 
            // C6
            // 
            this.C6.Location = new System.Drawing.Point(201, 282);
            this.C6.Name = "C6";
            this.C6.Size = new System.Drawing.Size(65, 20);
            this.C6.TabIndex = 53;
            this.C6.Click += new System.EventHandler(this.SelectCell);
            // 
            // B6
            // 
            this.B6.Location = new System.Drawing.Point(130, 282);
            this.B6.Name = "B6";
            this.B6.Size = new System.Drawing.Size(65, 20);
            this.B6.TabIndex = 52;
            this.B6.Click += new System.EventHandler(this.SelectCell);
            // 
            // A6
            // 
            this.A6.Location = new System.Drawing.Point(59, 282);
            this.A6.Name = "A6";
            this.A6.Size = new System.Drawing.Size(65, 20);
            this.A6.TabIndex = 51;
            this.A6.Click += new System.EventHandler(this.SelectCell);
            // 
            // J5
            // 
            this.J5.Location = new System.Drawing.Point(698, 256);
            this.J5.Name = "J5";
            this.J5.Size = new System.Drawing.Size(65, 20);
            this.J5.TabIndex = 50;
            this.J5.Click += new System.EventHandler(this.SelectCell);
            // 
            // I5
            // 
            this.I5.Location = new System.Drawing.Point(627, 256);
            this.I5.Name = "I5";
            this.I5.Size = new System.Drawing.Size(65, 20);
            this.I5.TabIndex = 49;
            this.I5.Click += new System.EventHandler(this.SelectCell);
            // 
            // H5
            // 
            this.H5.Location = new System.Drawing.Point(556, 256);
            this.H5.Name = "H5";
            this.H5.Size = new System.Drawing.Size(65, 20);
            this.H5.TabIndex = 48;
            this.H5.Click += new System.EventHandler(this.SelectCell);
            // 
            // G5
            // 
            this.G5.Location = new System.Drawing.Point(485, 256);
            this.G5.Name = "G5";
            this.G5.Size = new System.Drawing.Size(65, 20);
            this.G5.TabIndex = 47;
            this.G5.Click += new System.EventHandler(this.SelectCell);
            // 
            // F5
            // 
            this.F5.Location = new System.Drawing.Point(414, 256);
            this.F5.Name = "F5";
            this.F5.Size = new System.Drawing.Size(65, 20);
            this.F5.TabIndex = 46;
            this.F5.Click += new System.EventHandler(this.SelectCell);
            // 
            // E5
            // 
            this.E5.Location = new System.Drawing.Point(343, 256);
            this.E5.Name = "E5";
            this.E5.Size = new System.Drawing.Size(65, 20);
            this.E5.TabIndex = 45;
            this.E5.Click += new System.EventHandler(this.SelectCell);
            // 
            // D5
            // 
            this.D5.Location = new System.Drawing.Point(272, 256);
            this.D5.Name = "D5";
            this.D5.Size = new System.Drawing.Size(65, 20);
            this.D5.TabIndex = 44;
            this.D5.Click += new System.EventHandler(this.SelectCell);
            // 
            // C5
            // 
            this.C5.Location = new System.Drawing.Point(201, 256);
            this.C5.Name = "C5";
            this.C5.Size = new System.Drawing.Size(65, 20);
            this.C5.TabIndex = 43;
            this.C5.Click += new System.EventHandler(this.SelectCell);
            // 
            // B5
            // 
            this.B5.Location = new System.Drawing.Point(130, 256);
            this.B5.Name = "B5";
            this.B5.Size = new System.Drawing.Size(65, 20);
            this.B5.TabIndex = 42;
            this.B5.Click += new System.EventHandler(this.SelectCell);
            // 
            // A5
            // 
            this.A5.Location = new System.Drawing.Point(59, 256);
            this.A5.Name = "A5";
            this.A5.Size = new System.Drawing.Size(65, 20);
            this.A5.TabIndex = 41;
            this.A5.Click += new System.EventHandler(this.SelectCell);
            // 
            // J8
            // 
            this.J8.Location = new System.Drawing.Point(698, 334);
            this.J8.Name = "J8";
            this.J8.Size = new System.Drawing.Size(65, 20);
            this.J8.TabIndex = 80;
            this.J8.Click += new System.EventHandler(this.SelectCell);
            // 
            // I8
            // 
            this.I8.Location = new System.Drawing.Point(627, 334);
            this.I8.Name = "I8";
            this.I8.Size = new System.Drawing.Size(65, 20);
            this.I8.TabIndex = 79;
            this.I8.Click += new System.EventHandler(this.SelectCell);
            // 
            // H8
            // 
            this.H8.Location = new System.Drawing.Point(556, 334);
            this.H8.Name = "H8";
            this.H8.Size = new System.Drawing.Size(65, 20);
            this.H8.TabIndex = 78;
            this.H8.Click += new System.EventHandler(this.SelectCell);
            // 
            // G8
            // 
            this.G8.Location = new System.Drawing.Point(485, 334);
            this.G8.Name = "G8";
            this.G8.Size = new System.Drawing.Size(65, 20);
            this.G8.TabIndex = 77;
            this.G8.Click += new System.EventHandler(this.SelectCell);
            // 
            // F8
            // 
            this.F8.Location = new System.Drawing.Point(414, 334);
            this.F8.Name = "F8";
            this.F8.Size = new System.Drawing.Size(65, 20);
            this.F8.TabIndex = 76;
            this.F8.Click += new System.EventHandler(this.SelectCell);
            // 
            // E8
            // 
            this.E8.Location = new System.Drawing.Point(343, 334);
            this.E8.Name = "E8";
            this.E8.Size = new System.Drawing.Size(65, 20);
            this.E8.TabIndex = 75;
            this.E8.Click += new System.EventHandler(this.SelectCell);
            // 
            // D8
            // 
            this.D8.Location = new System.Drawing.Point(272, 334);
            this.D8.Name = "D8";
            this.D8.Size = new System.Drawing.Size(65, 20);
            this.D8.TabIndex = 74;
            this.D8.Click += new System.EventHandler(this.SelectCell);
            // 
            // C8
            // 
            this.C8.Location = new System.Drawing.Point(201, 334);
            this.C8.Name = "C8";
            this.C8.Size = new System.Drawing.Size(65, 20);
            this.C8.TabIndex = 73;
            this.C8.Click += new System.EventHandler(this.SelectCell);
            // 
            // B8
            // 
            this.B8.Location = new System.Drawing.Point(130, 334);
            this.B8.Name = "B8";
            this.B8.Size = new System.Drawing.Size(65, 20);
            this.B8.TabIndex = 72;
            this.B8.Click += new System.EventHandler(this.SelectCell);
            // 
            // A8
            // 
            this.A8.Location = new System.Drawing.Point(59, 334);
            this.A8.Name = "A8";
            this.A8.Size = new System.Drawing.Size(65, 20);
            this.A8.TabIndex = 71;
            this.A8.Click += new System.EventHandler(this.SelectCell);
            // 
            // J7
            // 
            this.J7.Location = new System.Drawing.Point(698, 308);
            this.J7.Name = "J7";
            this.J7.Size = new System.Drawing.Size(65, 20);
            this.J7.TabIndex = 70;
            this.J7.Click += new System.EventHandler(this.SelectCell);
            // 
            // I7
            // 
            this.I7.Location = new System.Drawing.Point(627, 308);
            this.I7.Name = "I7";
            this.I7.Size = new System.Drawing.Size(65, 20);
            this.I7.TabIndex = 69;
            this.I7.Click += new System.EventHandler(this.SelectCell);
            // 
            // H7
            // 
            this.H7.Location = new System.Drawing.Point(556, 308);
            this.H7.Name = "H7";
            this.H7.Size = new System.Drawing.Size(65, 20);
            this.H7.TabIndex = 68;
            this.H7.Click += new System.EventHandler(this.SelectCell);
            // 
            // G7
            // 
            this.G7.Location = new System.Drawing.Point(485, 308);
            this.G7.Name = "G7";
            this.G7.Size = new System.Drawing.Size(65, 20);
            this.G7.TabIndex = 67;
            this.G7.Click += new System.EventHandler(this.SelectCell);
            // 
            // F7
            // 
            this.F7.Location = new System.Drawing.Point(414, 308);
            this.F7.Name = "F7";
            this.F7.Size = new System.Drawing.Size(65, 20);
            this.F7.TabIndex = 66;
            this.F7.Click += new System.EventHandler(this.SelectCell);
            // 
            // E7
            // 
            this.E7.Location = new System.Drawing.Point(343, 308);
            this.E7.Name = "E7";
            this.E7.Size = new System.Drawing.Size(65, 20);
            this.E7.TabIndex = 65;
            this.E7.Click += new System.EventHandler(this.SelectCell);
            // 
            // D7
            // 
            this.D7.Location = new System.Drawing.Point(272, 308);
            this.D7.Name = "D7";
            this.D7.Size = new System.Drawing.Size(65, 20);
            this.D7.TabIndex = 64;
            this.D7.Click += new System.EventHandler(this.SelectCell);
            // 
            // C7
            // 
            this.C7.Location = new System.Drawing.Point(201, 308);
            this.C7.Name = "C7";
            this.C7.Size = new System.Drawing.Size(65, 20);
            this.C7.TabIndex = 63;
            this.C7.Click += new System.EventHandler(this.SelectCell);
            // 
            // B7
            // 
            this.B7.Location = new System.Drawing.Point(130, 308);
            this.B7.Name = "B7";
            this.B7.Size = new System.Drawing.Size(65, 20);
            this.B7.TabIndex = 62;
            this.B7.Click += new System.EventHandler(this.SelectCell);
            // 
            // A7
            // 
            this.A7.Location = new System.Drawing.Point(59, 308);
            this.A7.Name = "A7";
            this.A7.Size = new System.Drawing.Size(65, 20);
            this.A7.TabIndex = 61;
            this.A7.Click += new System.EventHandler(this.SelectCell);
            // 
            // J10
            // 
            this.J10.Location = new System.Drawing.Point(698, 386);
            this.J10.Name = "J10";
            this.J10.Size = new System.Drawing.Size(65, 20);
            this.J10.TabIndex = 100;
            this.J10.Click += new System.EventHandler(this.SelectCell);
            // 
            // I10
            // 
            this.I10.Location = new System.Drawing.Point(627, 386);
            this.I10.Name = "I10";
            this.I10.Size = new System.Drawing.Size(65, 20);
            this.I10.TabIndex = 99;
            this.I10.Click += new System.EventHandler(this.SelectCell);
            // 
            // H10
            // 
            this.H10.Location = new System.Drawing.Point(556, 386);
            this.H10.Name = "H10";
            this.H10.Size = new System.Drawing.Size(65, 20);
            this.H10.TabIndex = 98;
            this.H10.Click += new System.EventHandler(this.SelectCell);
            // 
            // G10
            // 
            this.G10.Location = new System.Drawing.Point(485, 386);
            this.G10.Name = "G10";
            this.G10.Size = new System.Drawing.Size(65, 20);
            this.G10.TabIndex = 97;
            this.G10.Click += new System.EventHandler(this.SelectCell);
            // 
            // F10
            // 
            this.F10.Location = new System.Drawing.Point(414, 386);
            this.F10.Name = "F10";
            this.F10.Size = new System.Drawing.Size(65, 20);
            this.F10.TabIndex = 96;
            this.F10.Click += new System.EventHandler(this.SelectCell);
            // 
            // E10
            // 
            this.E10.Location = new System.Drawing.Point(343, 386);
            this.E10.Name = "E10";
            this.E10.Size = new System.Drawing.Size(65, 20);
            this.E10.TabIndex = 95;
            this.E10.Click += new System.EventHandler(this.SelectCell);
            // 
            // D10
            // 
            this.D10.Location = new System.Drawing.Point(272, 386);
            this.D10.Name = "D10";
            this.D10.Size = new System.Drawing.Size(65, 20);
            this.D10.TabIndex = 94;
            this.D10.Click += new System.EventHandler(this.SelectCell);
            // 
            // C10
            // 
            this.C10.Location = new System.Drawing.Point(201, 386);
            this.C10.Name = "C10";
            this.C10.Size = new System.Drawing.Size(65, 20);
            this.C10.TabIndex = 93;
            this.C10.Click += new System.EventHandler(this.SelectCell);
            // 
            // B10
            // 
            this.B10.Location = new System.Drawing.Point(130, 386);
            this.B10.Name = "B10";
            this.B10.Size = new System.Drawing.Size(65, 20);
            this.B10.TabIndex = 92;
            this.B10.Click += new System.EventHandler(this.SelectCell);
            // 
            // A10
            // 
            this.A10.Location = new System.Drawing.Point(59, 386);
            this.A10.Name = "A10";
            this.A10.Size = new System.Drawing.Size(65, 20);
            this.A10.TabIndex = 91;
            this.A10.Click += new System.EventHandler(this.SelectCell);
            // 
            // J9
            // 
            this.J9.Location = new System.Drawing.Point(698, 360);
            this.J9.Name = "J9";
            this.J9.Size = new System.Drawing.Size(65, 20);
            this.J9.TabIndex = 90;
            this.J9.Click += new System.EventHandler(this.SelectCell);
            // 
            // I9
            // 
            this.I9.Location = new System.Drawing.Point(627, 360);
            this.I9.Name = "I9";
            this.I9.Size = new System.Drawing.Size(65, 20);
            this.I9.TabIndex = 89;
            this.I9.Click += new System.EventHandler(this.SelectCell);
            // 
            // H9
            // 
            this.H9.Location = new System.Drawing.Point(556, 360);
            this.H9.Name = "H9";
            this.H9.Size = new System.Drawing.Size(65, 20);
            this.H9.TabIndex = 88;
            this.H9.Click += new System.EventHandler(this.SelectCell);
            // 
            // G9
            // 
            this.G9.Location = new System.Drawing.Point(485, 360);
            this.G9.Name = "G9";
            this.G9.Size = new System.Drawing.Size(65, 20);
            this.G9.TabIndex = 87;
            this.G9.Click += new System.EventHandler(this.SelectCell);
            // 
            // F9
            // 
            this.F9.Location = new System.Drawing.Point(414, 360);
            this.F9.Name = "F9";
            this.F9.Size = new System.Drawing.Size(65, 20);
            this.F9.TabIndex = 86;
            this.F9.Click += new System.EventHandler(this.SelectCell);
            // 
            // E9
            // 
            this.E9.Location = new System.Drawing.Point(343, 360);
            this.E9.Name = "E9";
            this.E9.Size = new System.Drawing.Size(65, 20);
            this.E9.TabIndex = 85;
            this.E9.Click += new System.EventHandler(this.SelectCell);
            // 
            // D9
            // 
            this.D9.Location = new System.Drawing.Point(272, 360);
            this.D9.Name = "D9";
            this.D9.Size = new System.Drawing.Size(65, 20);
            this.D9.TabIndex = 84;
            this.D9.Click += new System.EventHandler(this.SelectCell);
            // 
            // C9
            // 
            this.C9.Location = new System.Drawing.Point(201, 360);
            this.C9.Name = "C9";
            this.C9.Size = new System.Drawing.Size(65, 20);
            this.C9.TabIndex = 83;
            this.C9.Click += new System.EventHandler(this.SelectCell);
            // 
            // B9
            // 
            this.B9.Location = new System.Drawing.Point(130, 360);
            this.B9.Name = "B9";
            this.B9.Size = new System.Drawing.Size(65, 20);
            this.B9.TabIndex = 82;
            this.B9.Click += new System.EventHandler(this.SelectCell);
            // 
            // A9
            // 
            this.A9.Location = new System.Drawing.Point(59, 360);
            this.A9.Name = "A9";
            this.A9.Size = new System.Drawing.Size(65, 20);
            this.A9.TabIndex = 81;
            this.A9.Click += new System.EventHandler(this.SelectCell);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(81, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 101;
            this.label1.Text = "A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(151, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 20);
            this.label2.TabIndex = 102;
            this.label2.Text = "B";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(294, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 20);
            this.label3.TabIndex = 104;
            this.label3.Text = "D";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(224, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 20);
            this.label4.TabIndex = 103;
            this.label4.Text = "C";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(435, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 20);
            this.label5.TabIndex = 106;
            this.label5.Text = "F";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(365, 129);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 20);
            this.label6.TabIndex = 105;
            this.label6.Text = "E";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(577, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 20);
            this.label7.TabIndex = 108;
            this.label7.Text = "H";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(507, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 20);
            this.label8.TabIndex = 107;
            this.label8.Text = "G";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(720, 129);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 20);
            this.label9.TabIndex = 110;
            this.label9.Text = "J";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(650, 129);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 20);
            this.label10.TabIndex = 109;
            this.label10.Text = "I";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(33, 152);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(18, 20);
            this.label11.TabIndex = 111;
            this.label11.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(33, 178);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 20);
            this.label12.TabIndex = 112;
            this.label12.Text = "2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(33, 230);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 20);
            this.label13.TabIndex = 114;
            this.label13.Text = "4";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(33, 204);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 20);
            this.label14.TabIndex = 113;
            this.label14.Text = "3";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(35, 282);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 20);
            this.label15.TabIndex = 116;
            this.label15.Text = "6";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(35, 256);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(18, 20);
            this.label16.TabIndex = 115;
            this.label16.Text = "5";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(35, 334);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(18, 20);
            this.label17.TabIndex = 118;
            this.label17.Text = "8";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(35, 308);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(18, 20);
            this.label18.TabIndex = 117;
            this.label18.Text = "7";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(26, 384);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 20);
            this.label19.TabIndex = 120;
            this.label19.Text = "10";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(35, 357);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 20);
            this.label20.TabIndex = 119;
            this.label20.Text = "9";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label22.Location = new System.Drawing.Point(55, 93);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 20);
            this.label22.TabIndex = 121;
            this.label22.Text = "Formuła:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.edytujToolStripMenuItem,
            this.pomocToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(815, 24);
            this.menuStrip1.TabIndex = 122;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nowyToolStripMenuItem,
            this.otwórzToolStripMenuItem,
            this.zapiszToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // nowyToolStripMenuItem
            // 
            this.nowyToolStripMenuItem.Name = "nowyToolStripMenuItem";
            this.nowyToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.nowyToolStripMenuItem.Text = "Nowy";
            this.nowyToolStripMenuItem.Click += new System.EventHandler(this.nowyToolStripMenuItem_Click);
            // 
            // otwórzToolStripMenuItem
            // 
            this.otwórzToolStripMenuItem.Name = "otwórzToolStripMenuItem";
            this.otwórzToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.otwórzToolStripMenuItem.Text = "Otwórz";
            this.otwórzToolStripMenuItem.Click += new System.EventHandler(this.otwórzToolStripMenuItem_Click);
            // 
            // zapiszToolStripMenuItem
            // 
            this.zapiszToolStripMenuItem.Name = "zapiszToolStripMenuItem";
            this.zapiszToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.zapiszToolStripMenuItem.Text = "Zapisz";
            this.zapiszToolStripMenuItem.Click += new System.EventHandler(this.zapiszToolStripMenuItem_Click);
            // 
            // edytujToolStripMenuItem
            // 
            this.edytujToolStripMenuItem.Name = "edytujToolStripMenuItem";
            this.edytujToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.edytujToolStripMenuItem.Text = "Edytuj";
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.pomocToolStripMenuItem.Text = "Pomoc";
            this.pomocToolStripMenuItem.Click += new System.EventHandler(this.pomocToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(59, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 25);
            this.button1.TabIndex = 123;
            this.button1.Text = "B";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(102, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 25);
            this.button2.TabIndex = 124;
            this.button2.Text = "I";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Red;
            this.pictureBox1.Location = new System.Drawing.Point(145, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 24);
            this.pictureBox1.TabIndex = 125;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Green;
            this.pictureBox2.Location = new System.Drawing.Point(187, 55);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 24);
            this.pictureBox2.TabIndex = 126;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Blue;
            this.pictureBox3.Location = new System.Drawing.Point(228, 55);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(25, 24);
            this.pictureBox3.TabIndex = 127;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox4.Location = new System.Drawing.Point(272, 55);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(25, 24);
            this.pictureBox4.TabIndex = 128;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ExcelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 483);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.J10);
            this.Controls.Add(this.I10);
            this.Controls.Add(this.H10);
            this.Controls.Add(this.G10);
            this.Controls.Add(this.F10);
            this.Controls.Add(this.E10);
            this.Controls.Add(this.D10);
            this.Controls.Add(this.C10);
            this.Controls.Add(this.B10);
            this.Controls.Add(this.A10);
            this.Controls.Add(this.J9);
            this.Controls.Add(this.I9);
            this.Controls.Add(this.H9);
            this.Controls.Add(this.G9);
            this.Controls.Add(this.F9);
            this.Controls.Add(this.E9);
            this.Controls.Add(this.D9);
            this.Controls.Add(this.C9);
            this.Controls.Add(this.B9);
            this.Controls.Add(this.A9);
            this.Controls.Add(this.J8);
            this.Controls.Add(this.I8);
            this.Controls.Add(this.H8);
            this.Controls.Add(this.G8);
            this.Controls.Add(this.F8);
            this.Controls.Add(this.E8);
            this.Controls.Add(this.D8);
            this.Controls.Add(this.C8);
            this.Controls.Add(this.B8);
            this.Controls.Add(this.A8);
            this.Controls.Add(this.J7);
            this.Controls.Add(this.I7);
            this.Controls.Add(this.H7);
            this.Controls.Add(this.G7);
            this.Controls.Add(this.F7);
            this.Controls.Add(this.E7);
            this.Controls.Add(this.D7);
            this.Controls.Add(this.C7);
            this.Controls.Add(this.B7);
            this.Controls.Add(this.A7);
            this.Controls.Add(this.J6);
            this.Controls.Add(this.I6);
            this.Controls.Add(this.H6);
            this.Controls.Add(this.G6);
            this.Controls.Add(this.F6);
            this.Controls.Add(this.E6);
            this.Controls.Add(this.D6);
            this.Controls.Add(this.C6);
            this.Controls.Add(this.B6);
            this.Controls.Add(this.A6);
            this.Controls.Add(this.J5);
            this.Controls.Add(this.I5);
            this.Controls.Add(this.H5);
            this.Controls.Add(this.G5);
            this.Controls.Add(this.F5);
            this.Controls.Add(this.E5);
            this.Controls.Add(this.D5);
            this.Controls.Add(this.C5);
            this.Controls.Add(this.B5);
            this.Controls.Add(this.A5);
            this.Controls.Add(this.J4);
            this.Controls.Add(this.I4);
            this.Controls.Add(this.H4);
            this.Controls.Add(this.G4);
            this.Controls.Add(this.F4);
            this.Controls.Add(this.E4);
            this.Controls.Add(this.D4);
            this.Controls.Add(this.C4);
            this.Controls.Add(this.B4);
            this.Controls.Add(this.A4);
            this.Controls.Add(this.J3);
            this.Controls.Add(this.I3);
            this.Controls.Add(this.H3);
            this.Controls.Add(this.G3);
            this.Controls.Add(this.F3);
            this.Controls.Add(this.E3);
            this.Controls.Add(this.D3);
            this.Controls.Add(this.C3);
            this.Controls.Add(this.B3);
            this.Controls.Add(this.A3);
            this.Controls.Add(this.J2);
            this.Controls.Add(this.I2);
            this.Controls.Add(this.H2);
            this.Controls.Add(this.G2);
            this.Controls.Add(this.F2);
            this.Controls.Add(this.E2);
            this.Controls.Add(this.D2);
            this.Controls.Add(this.C2);
            this.Controls.Add(this.B2);
            this.Controls.Add(this.A2);
            this.Controls.Add(this.J1);
            this.Controls.Add(this.I1);
            this.Controls.Add(this.H1);
            this.Controls.Add(this.G1);
            this.Controls.Add(this.F1);
            this.Controls.Add(this.E1);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.C1);
            this.Controls.Add(this.B1);
            this.Controls.Add(this.A1);
            this.Controls.Add(this.formulaBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ExcelForm";
            this.Text = "Arkusz Kalkulacyjny 2018";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox formulaBox;
        private System.Windows.Forms.TextBox A1;
        private System.Windows.Forms.TextBox B1;
        private System.Windows.Forms.TextBox D1;
        private System.Windows.Forms.TextBox C1;
        private System.Windows.Forms.TextBox H1;
        private System.Windows.Forms.TextBox G1;
        private System.Windows.Forms.TextBox F1;
        private System.Windows.Forms.TextBox E1;
        private System.Windows.Forms.TextBox J1;
        private System.Windows.Forms.TextBox I1;
        private System.Windows.Forms.TextBox J2;
        private System.Windows.Forms.TextBox I2;
        private System.Windows.Forms.TextBox H2;
        private System.Windows.Forms.TextBox G2;
        private System.Windows.Forms.TextBox F2;
        private System.Windows.Forms.TextBox E2;
        private System.Windows.Forms.TextBox D2;
        private System.Windows.Forms.TextBox C2;
        private System.Windows.Forms.TextBox B2;
        private System.Windows.Forms.TextBox A2;
        private System.Windows.Forms.TextBox J4;
        private System.Windows.Forms.TextBox I4;
        private System.Windows.Forms.TextBox H4;
        private System.Windows.Forms.TextBox G4;
        private System.Windows.Forms.TextBox F4;
        private System.Windows.Forms.TextBox E4;
        private System.Windows.Forms.TextBox D4;
        private System.Windows.Forms.TextBox C4;
        private System.Windows.Forms.TextBox B4;
        private System.Windows.Forms.TextBox A4;
        private System.Windows.Forms.TextBox J3;
        private System.Windows.Forms.TextBox I3;
        private System.Windows.Forms.TextBox H3;
        private System.Windows.Forms.TextBox G3;
        private System.Windows.Forms.TextBox F3;
        private System.Windows.Forms.TextBox E3;
        private System.Windows.Forms.TextBox D3;
        private System.Windows.Forms.TextBox C3;
        private System.Windows.Forms.TextBox B3;
        private System.Windows.Forms.TextBox A3;
        private System.Windows.Forms.TextBox J6;
        private System.Windows.Forms.TextBox I6;
        private System.Windows.Forms.TextBox H6;
        private System.Windows.Forms.TextBox G6;
        private System.Windows.Forms.TextBox F6;
        private System.Windows.Forms.TextBox E6;
        private System.Windows.Forms.TextBox D6;
        private System.Windows.Forms.TextBox C6;
        private System.Windows.Forms.TextBox B6;
        private System.Windows.Forms.TextBox A6;
        private System.Windows.Forms.TextBox J5;
        private System.Windows.Forms.TextBox I5;
        private System.Windows.Forms.TextBox H5;
        private System.Windows.Forms.TextBox G5;
        private System.Windows.Forms.TextBox F5;
        private System.Windows.Forms.TextBox E5;
        private System.Windows.Forms.TextBox D5;
        private System.Windows.Forms.TextBox C5;
        private System.Windows.Forms.TextBox B5;
        private System.Windows.Forms.TextBox A5;
        private System.Windows.Forms.TextBox J8;
        private System.Windows.Forms.TextBox I8;
        private System.Windows.Forms.TextBox H8;
        private System.Windows.Forms.TextBox G8;
        private System.Windows.Forms.TextBox F8;
        private System.Windows.Forms.TextBox E8;
        private System.Windows.Forms.TextBox D8;
        private System.Windows.Forms.TextBox C8;
        private System.Windows.Forms.TextBox B8;
        private System.Windows.Forms.TextBox A8;
        private System.Windows.Forms.TextBox J7;
        private System.Windows.Forms.TextBox I7;
        private System.Windows.Forms.TextBox H7;
        private System.Windows.Forms.TextBox G7;
        private System.Windows.Forms.TextBox F7;
        private System.Windows.Forms.TextBox E7;
        private System.Windows.Forms.TextBox D7;
        private System.Windows.Forms.TextBox C7;
        private System.Windows.Forms.TextBox B7;
        private System.Windows.Forms.TextBox A7;
        private System.Windows.Forms.TextBox J10;
        private System.Windows.Forms.TextBox I10;
        private System.Windows.Forms.TextBox H10;
        private System.Windows.Forms.TextBox G10;
        private System.Windows.Forms.TextBox F10;
        private System.Windows.Forms.TextBox E10;
        private System.Windows.Forms.TextBox D10;
        private System.Windows.Forms.TextBox C10;
        private System.Windows.Forms.TextBox B10;
        private System.Windows.Forms.TextBox A10;
        private System.Windows.Forms.TextBox J9;
        private System.Windows.Forms.TextBox I9;
        private System.Windows.Forms.TextBox H9;
        private System.Windows.Forms.TextBox G9;
        private System.Windows.Forms.TextBox F9;
        private System.Windows.Forms.TextBox E9;
        private System.Windows.Forms.TextBox D9;
        private System.Windows.Forms.TextBox C9;
        private System.Windows.Forms.TextBox B9;
        private System.Windows.Forms.TextBox A9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ToolStripMenuItem nowyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otwórzToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zapiszToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

