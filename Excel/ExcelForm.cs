﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Excel
{
    public partial class ExcelForm : Form
    {
        public ExcelForm()
        {
            InitializeComponent();
        }
        public List<string> listOfFormatted = new List<string>();


        private void SelectCell(object sender, EventArgs e)
        {
            TextBox cellTextBox = sender as TextBox;
            CellSelected.name = cellTextBox.Name;
            cellTextBox.BackColor = Color.Azure;
            formulaBox.Text = cellTextBox.Text;

            for(int j = 65;j<=74;j++)
            {
                for (int i = 1; i <= 10; i++)
                {
                    string TextBoxName = (char)j + Convert.ToString(i);
                    Control[] TextBoxControl = this.Controls.Find(TextBoxName, true);
                    bool doNotFormat = false;
                    try
                    {
                        for(int k = 0; k < listOfFormatted.Count;k++)
                        {
                            if (listOfFormatted[k] == TextBoxControl[0].Name)
                                doNotFormat = true;
                        }
                        if (TextBoxControl[0].Name != cellTextBox.Name && doNotFormat == false )
                            TextBoxControl[0].BackColor = Color.White;
                    }
                    catch
                    {

                    }
                    
                }
            }
        }

        private void CheckFormula(object sender, EventArgs e)
        {

        }



        private void Form1_Load(object sender, EventArgs e)
        {
            //
        }

        private void formulaBox_TextChanged(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].Text = formulaBox.Text;
            FormulaValidation formulaValidation = new FormulaValidation();
            try
            {
                if (formulaValidation.IsFormula(TextBoxControl[0].Text))
                {
                   if(TextBoxControl[0].Text.Substring(1, 4) == "SUMA" || TextBoxControl[0].Text.Substring(1, 4) == "suma")
                   {
                        Cell.list.Add(new ValueFormula(TextBoxControl[0].Text, formulaBox.Text, "A", "1"));


                        if(TextBoxControl[0].Text.Substring(8, 1) == ",")
                        {
                            double sum = 0;
                            string a, b;
                            a = TextBoxControl[0].Text.Substring(6, 2);
                            b = TextBoxControl[0].Text.Substring(9, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);
                            Control[] TextBoxControlB = this.Controls.Find(b, true);
                            double aValue=0, bValue=0;
                            if (formulaValidation.IsNumber(TextBoxControlA[0].Text))
                                aValue = Convert.ToDouble(TextBoxControlA[0].Text);
                            else
                                MessageBox.Show("Podano tekst");

                            if (formulaValidation.IsNumber(TextBoxControlB[0].Text))
                                bValue = Convert.ToDouble(TextBoxControlB[0].Text);
                            else
                                MessageBox.Show("Podano tekst");
                            sum = aValue + bValue;

                            TextBoxControl[0].Text = Convert.ToString(sum);

                        }
                        if (TextBoxControl[0].Text.Substring(8, 1) == ":")
                        {
                            double sum = 0;
                            string a, b, colName;
                            colName = TextBoxControl[0].Text.Substring(6, 1);
                            a = TextBoxControl[0].Text.Substring(6, 2);
                            b = TextBoxControl[0].Text.Substring(9, 2);
                            int start, stop;
                            start = Convert.ToInt32(TextBoxControl[0].Text.Substring(7, 1));
                            stop = Convert.ToInt32(TextBoxControl[0].Text.Substring(10, 1));

                            for(int i = start; i<=stop;i++)
                            {
                                Control[] TextBoxControlSubset = this.Controls.Find(colName + Convert.ToString(i), true);
                                if (formulaValidation.IsNumber(TextBoxControlSubset[0].Text))
                                    sum = sum + Convert.ToDouble(TextBoxControlSubset[0].Text);
                                else
                                {
                                    MessageBox.Show("Podano tekst");
                                    break;
                                }
                            }

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }

                    }
                   if (TextBoxControl[0].Text.Substring(1, 7) == "ROZNICA" || TextBoxControl[0].Text.Substring(1, 7) == "roznica")
                   {
                        if (TextBoxControl[0].Text.Substring(11, 1) == ",")
                        {
                            double sum = 0;
                            string a, b;
                            a = TextBoxControl[0].Text.Substring(9, 2);
                            b = TextBoxControl[0].Text.Substring(12, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);
                            Control[] TextBoxControlB = this.Controls.Find(b, true);
                            
                            double aValue = 0, bValue = 0;
                            if (formulaValidation.IsNumber(TextBoxControlA[0].Text))
                                aValue = Convert.ToDouble(TextBoxControlA[0].Text);
                            else
                                MessageBox.Show("Podano tekst");

                            if (formulaValidation.IsNumber(TextBoxControlB[0].Text))
                                bValue = Convert.ToDouble(TextBoxControlB[0].Text);
                            else
                                MessageBox.Show("Podano tekst");
                            sum = aValue - bValue;

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                        if (TextBoxControl[0].Text.Substring(11, 1) == ":")
                        {
                            //=iloczyn(A1:A3)
                            string a, b, colName;
                            colName = TextBoxControl[0].Text.Substring(9, 1);
                            a = TextBoxControl[0].Text.Substring(9, 2);
                            b = TextBoxControl[0].Text.Substring(12, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);

                            double sum = Convert.ToDouble(TextBoxControlA[0].Text);
                            int start, stop;
                            start = Convert.ToInt32(TextBoxControl[0].Text.Substring(10, 1));
                            stop = Convert.ToInt32(TextBoxControl[0].Text.Substring(13, 1));
                            for (int i = start + 1; i <= stop; i++)
                            {
                                Control[] TextBoxControlSubset = this.Controls.Find(colName + Convert.ToString(i), true);
                                if (formulaValidation.IsNumber(TextBoxControlSubset[0].Text))
                                    sum = sum - Convert.ToDouble(TextBoxControlSubset[0].Text);
                                else
                                {
                                    MessageBox.Show("Podano tekst");
                                    break;
                                }
                            }

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                    }
                   if (TextBoxControl[0].Text.Substring(1, 7) == "ILOCZYN" || TextBoxControl[0].Text.Substring(1, 7) == "iloczyn")
                   {
                        if (TextBoxControl[0].Text.Substring(11, 1) == ",")
                        {
                            double sum = 0;
                            string a, b;
                            a = TextBoxControl[0].Text.Substring(9, 2);
                            b = TextBoxControl[0].Text.Substring(12, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);
                            Control[] TextBoxControlB = this.Controls.Find(b, true);
                            double aValue = 0, bValue = 0;
                            if (formulaValidation.IsNumber(TextBoxControlA[0].Text))
                                aValue = Convert.ToDouble(TextBoxControlA[0].Text);
                            else
                                MessageBox.Show("Podano tekst");

                            if (formulaValidation.IsNumber(TextBoxControlB[0].Text))
                                bValue = Convert.ToDouble(TextBoxControlB[0].Text);
                            else
                                MessageBox.Show("Podano tekst");
                            sum = aValue * bValue;

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                        if (TextBoxControl[0].Text.Substring(11, 1) == ":")
                        {
                            //=iloczyn(A1:A3)
                            string a, b, colName;
                            colName = TextBoxControl[0].Text.Substring(9, 1);
                            a = TextBoxControl[0].Text.Substring(9, 2);
                            b = TextBoxControl[0].Text.Substring(12, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);

                            double sum = Convert.ToDouble(TextBoxControlA[0].Text);
                            int start, stop;
                            start = Convert.ToInt32(TextBoxControl[0].Text.Substring(10, 1));
                            stop = Convert.ToInt32(TextBoxControl[0].Text.Substring(13, 1));
                            for (int i = start+1; i <= stop; i++)
                            {
                                Control[] TextBoxControlSubset = this.Controls.Find(colName + Convert.ToString(i), true);
                                if (formulaValidation.IsNumber(TextBoxControlSubset[0].Text))
                                    sum = sum * Convert.ToDouble(TextBoxControlSubset[0].Text);
                                else
                                {
                                    MessageBox.Show("Podano tekst");
                                    break;
                                }
                            }

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                    }
                    if (TextBoxControl[0].Text.Substring(1, 6) == "ILORAZ" || TextBoxControl[0].Text.Substring(1, 6) == "iloraz")
                    {
                        if (TextBoxControl[0].Text.Substring(10, 1) == ",")
                        {
                            double sum = 0;
                            string a, b;
                            a = TextBoxControl[0].Text.Substring(8, 2);
                            b = TextBoxControl[0].Text.Substring(11, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);
                            Control[] TextBoxControlB = this.Controls.Find(b, true);
                            double aValue = 0, bValue = 0;
                            if (formulaValidation.IsNumber(TextBoxControlA[0].Text))
                                aValue = Convert.ToDouble(TextBoxControlA[0].Text);
                            else
                                MessageBox.Show("Podano tekst");

                            if (formulaValidation.IsNumber(TextBoxControlB[0].Text))
                                bValue = Convert.ToDouble(TextBoxControlB[0].Text);
                            else
                                MessageBox.Show("Podano tekst");
                            sum = aValue - bValue;

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                        if (TextBoxControl[0].Text.Substring(10, 1) == ":")
                        {
                            //=iloczyn(A1:A3)
                            string a, b, colName;
                            colName = TextBoxControl[0].Text.Substring(8, 1);
                            a = TextBoxControl[0].Text.Substring(8, 2);
                            b = TextBoxControl[0].Text.Substring(11, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);

                            double sum = Convert.ToDouble(TextBoxControlA[0].Text);
                            int start, stop;
                            start = Convert.ToInt32(TextBoxControl[0].Text.Substring(9, 1));
                            stop = Convert.ToInt32(TextBoxControl[0].Text.Substring(12, 1));
                            for (int i = start + 1; i <= stop; i++)
                            {
                                Control[] TextBoxControlSubset = this.Controls.Find(colName + Convert.ToString(i), true);
                                if (formulaValidation.IsNumber(TextBoxControlSubset[0].Text))
                                    sum = sum / Convert.ToDouble(TextBoxControlSubset[0].Text);
                                else
                                {
                                    MessageBox.Show("Podano tekst");
                                    break;
                                }
                            }

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                    }
                    if (TextBoxControl[0].Text.Substring(1, 7) == "SREDNIA" || TextBoxControl[0].Text.Substring(1, 7) == "srednia")
                    {
                        if (TextBoxControl[0].Text.Substring(11, 1) == ",")
                        {
                            double sum = 0;
                            string a, b;
                            a = TextBoxControl[0].Text.Substring(9, 2);
                            b = TextBoxControl[0].Text.Substring(12, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);
                            Control[] TextBoxControlB = this.Controls.Find(b, true);
                            double aValue = 0, bValue = 0;
                            if (formulaValidation.IsNumber(TextBoxControlA[0].Text))
                                aValue = Convert.ToDouble(TextBoxControlA[0].Text);
                            else
                                MessageBox.Show("Podano tekst");

                            if (formulaValidation.IsNumber(TextBoxControlB[0].Text))
                                bValue = Convert.ToDouble(TextBoxControlB[0].Text);
                            else
                                MessageBox.Show("Podano tekst");
                            sum = (aValue + bValue)/2;

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                        if (TextBoxControl[0].Text.Substring(11, 1) == ":")
                        {
                            //=iloczyn(A1:A3)
                            string a, b, colName;
                            colName = TextBoxControl[0].Text.Substring(9, 1);
                            a = TextBoxControl[0].Text.Substring(9, 2);
                            b = TextBoxControl[0].Text.Substring(12, 2);
                            Control[] TextBoxControlA = this.Controls.Find(a, true);

                            double sum = Convert.ToDouble(TextBoxControlA[0].Text);
                            int start, stop;
                            start = Convert.ToInt32(TextBoxControl[0].Text.Substring(10, 1));
                            stop = Convert.ToInt32(TextBoxControl[0].Text.Substring(13, 1));
                            for (int i = start + 1; i <= stop; i++)
                            {
                                Control[] TextBoxControlSubset = this.Controls.Find(colName + Convert.ToString(i), true);
                                if (formulaValidation.IsNumber(TextBoxControlSubset[0].Text))
                                    sum = sum + Convert.ToDouble(TextBoxControlSubset[0].Text);
                                else
                                {
                                    MessageBox.Show("Podano tekst");
                                    break;
                                }
                            }
                            sum = sum / (stop - start + 1);

                            TextBoxControl[0].Text = Convert.ToString(sum);
                        }
                    }

                }
            }
            catch
            {

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].BackColor = Color.Red;
            listOfFormatted.Add(Convert.ToString(TextBoxControl[0].Name));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].BackColor = Color.Green;
            listOfFormatted.Add(Convert.ToString(TextBoxControl[0].Name));
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].BackColor = Color.Blue;
            listOfFormatted.Add(Convert.ToString(TextBoxControl[0].Name));
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].BackColor = Color.Yellow;
            listOfFormatted.Add(Convert.ToString(TextBoxControl[0].Name));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].Font = new Font(TextBoxControl[0].Font, FontStyle.Bold);
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Control[] TextBoxControl = this.Controls.Find(CellSelected.name, true);
            TextBoxControl[0].Font = new Font(TextBoxControl[0].Font, FontStyle.Italic);
        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                XmlTextWriter Writer = new XmlTextWriter(saveFileDialog1.FileName, null);
                Writer.WriteStartDocument();
                Writer.WriteStartElement("root");
                for (int i = 65; i <= 74; i++)
                {
                    Writer.WriteStartElement("col");
                    Writer.WriteAttributeString("id", Convert.ToString((char)i));
                    for (int j = 1; j <= 10; j++)
                    {
                        Writer.WriteStartElement("row");
                        Writer.WriteAttributeString("id", Convert.ToString(j));

                        Control[] TextBoxControl = this.Controls.Find((char)i + Convert.ToString(j), true);

                        Writer.WriteAttributeString("color", Convert.ToString(TextBoxControl[0].BackColor.ToString()));
                        Writer.WriteString(TextBoxControl[0].Text);
                        Writer.WriteEndElement();
                    }



                    Writer.WriteEndElement();
                }
                Writer.WriteEndElement();
                Writer.WriteEndDocument();
                Writer.Close();
            }
        }

        private void otwórzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                XmlNode Node;
                XmlDocument XmlDoc = new XmlDocument();
                try
                {
                    XmlDoc.Load(openFileDialog1.FileName);
                    int Count = XmlDoc.GetElementsByTagName("col").Count;
                    for (int i = 1; i <= Count; i++)
                    {

                        for (int j = 1 + ((i * 10) - 10); j <= (i * 10); j++)
                        {

                            if (j == 1)
                            {
                                Node = XmlDoc.GetElementsByTagName("row").Item(j);
                                Control[] TextBoxControl = this.Controls.Find((char)(i + 64) + Convert.ToString(j), true);
                                TextBoxControl[0].Text = XmlDoc.GetElementsByTagName("row").Item(j).InnerText;
                            }
                            else
                            {
                                if (j == 100)
                                    break;
                                Node = XmlDoc.GetElementsByTagName("row").Item(j - 1);
                                Control[] TextBoxControl = this.Controls.Find((char)(i + 64) + Convert.ToString(j - ((i - 1) * 10)), true);
                                TextBoxControl[0].Text = XmlDoc.GetElementsByTagName("row").Item(j - 1).InnerText;
                                if (Node.Attributes.GetNamedItem("color").InnerText == "Color [White]")
                                    TextBoxControl[0].BackColor = Color.White;
                                if (Node.Attributes.GetNamedItem("color").InnerText == "Color [Blue]")
                                    TextBoxControl[0].BackColor = Color.Blue;
                                if (Node.Attributes.GetNamedItem("color").InnerText == "Color [Green]")
                                    TextBoxControl[0].BackColor = Color.Green;
                                if (Node.Attributes.GetNamedItem("color").InnerText == "Color [Red]")
                                    TextBoxControl[0].BackColor = Color.Red;
                                if (Node.Attributes.GetNamedItem("color").InnerText == "Color [Yellow]")
                                    TextBoxControl[0].BackColor = Color.Yellow;
                            }



                        }


                    }
                }
                catch (XmlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void pomocToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Inżyniernia Oprogramowania\nKrzysztof Rybicki\nstyczeń 2019");
        }

        private void nowyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for(int i = 65;i<=74;i++)
            {
                for(int j=1;j<=10;j++)
                {
                    Control[] TextBoxControl = this.Controls.Find((char)(i) + Convert.ToString(j), true);
                    TextBoxControl[0].Text = "";
                    TextBoxControl[0].BackColor = Color.White;

                }
            }
        }
    }
}
